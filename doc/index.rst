
Welcome to Dynac's documentation!
=================================

The computer code DYNAC contains a set of very accurate quasi-Liouvillian beam dynamics equations, introduced in 1994. It is applicable to protons, heavy ions and non-relativistic electrons. Long accelerating elements with complex and/or asymmetric longitudinal electromagnetic fields (e.g. superconducting cavities) may be treated as one cavity (in stead of individual cells). This is achieved through the concept of the "equivalent accelerating field" [1].

The V6.0R8 version already supported multi-charge state beams for a variety of beam line elements. This capability has been benchmarked against other beam dynamics codes [2]. In the V6.0R9 version, the electrostatic dipole has been added. In the following versions there are various enhancements, such as an improved treatment of the RMS section in an RFQ and some corrections. Further benchmarking is reported in [3] and [4].

DYNAC has several space charge routines, including a 3D space charge routine called HERSC [5]. This routine derives from the typical procedure adopted in mathematical physics; the problem is transposed from some point to point correspondence onto a functional space spanned by a finite sequence of 3D Hermite functions, where the analytical set of beam self-field equations is found without any sort of restriction or basic hypothesis.

REFERENCES

 1. `LINAC2002 Conference paper on DYNAC (PDF format) <http://dynac.web.cern.ch/dynac/LINAC2002_TH429.pdf>`_
 2. `LINAC2012 Conference paper on DYNAC (PDF format) <http://dynac.web.cern.ch/dynac/LINAC2012_THPB065.pdf>`_
 3. `LINAC2014 Conference paper on DYNAC (PDF format) <http://dynac.web.cern.ch/dynac/LINAC2014_THPP043.pdf>`_
 4. `LINAC2016 Conference paper on DYNAC (PDF format) <http://dynac.web.cern.ch/dynac/LINAC2016_MOPRC007.pdf>`_
 5. `LINAC2002 Conference paper on HERSC (PDF format) <http://dynac.web.cern.ch/dynac/LINAC2002_TH424.pdf>`_

The source code of Dynac can be found at `<https://gitlab.com/dynac-sim/dynac>`_

Contents:

.. toctree::
   :maxdepth: 2

   documentation/index
   API <api/html/index.html#http://>

This version of the documentation is currently work in progress. For a complete documentation please see the `pdf verison <dynac_UG_V6R16_2016_09_15.pdf>`_.

