Input Files
===========

With respect to previous versions of DYNAC, changes have been made in V6.0 enabling an easier definition of the input files at run-time.
The format for starting execution of the code is based on an extension in the GFORTRAN compiler and is as follows:
`dynacv6_0 [-h] [-mingw] [filen]`
where

 1) filen is the file name of the DYNAC input file containing input data typifying the accelerator and/or transport line; this file is mandatory (Unit 7).
 2) -h causes some 'help' information to be printed on the screen
 3) -mingw can be used as an option when using certain MINGW versions on MSWINDOWS. This may be required as certain gfortran intrinsics behave differently (e.g. "ctime") depending on the gfortran version.

The first line of the DYNAC input file 'filen' is a mandatory comment line; this can be used to give a brief description of what the input file represents.

All other input files (e.g. optional files describing particle distribution, solenoid field, electromagnetic field etc) are defined within the DYNAC input file. The formats of these are described in the corresponding type code entries in this user guide.
