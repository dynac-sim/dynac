Optical Lenses
==============


BMAGNET
-------

Transport throughout a bending magnet (second-order matrix formalism if SECORD_ card is preceding this one in the command list).

ENTRY:
 1) NSEC

    NSEC: number of sectors in which the dipole is to be divided. In the case of a multi-charge state beam in the magnet and/or with the occurrence of space charge computations in the magnet, this parameter is required to be larger than 1 (see remarks).

 2) ANGL, RMO, BAIM, XN, XB

    ANGL: bending angle of the central trajectory (deg) RMO: bending radius of the central trajectory (cm)

    BAIM: magnetic field (kG)
      - If BAIM = 0.0, the rigidity and field will be calculated based on the reference particle.
      - If BAIM > 0.0, the rigidity will be based on the field entered here.

    XN: field gradient n (dimensionless parameter corresponding to the parameter n in the code TRANSPORT, see remark (1))

    XB: second order field gradient beta (dimensionless parameter corresponding to the parameter beta in the code TRANSORT, see remark (1))

 3) PENT1 RAB1 EK1 EK2 APB(1)

    PENT1: angle of pole-face rotation at the entrance (deg), see remark (2) for conventions.

    RAB1: curvature of the entrance pole-face (cm), see remark (2) for conventions

    EK1: An integral related to the extent of the fringe field (dimensionless parameter corresponding to the parameter K1 in the code TRANSPORT, see remark 3).

    EK2: A second integral related to the extent of the fringe field (dimensionless parameter corresponding to the parameter K2 in the code TRANSPORT, see remark 3).

    APB(1): vertical half aperture at the entrance (cm) 

 4) PENT2 RAB2 SK1 SK2 APB(2)

    PENT2: angle of pole-face rotation at the exit (deg), see remark (2) for conventions. RAB2: curvature of the exit pole-face (cm)

    SK1: An integral related to the extent of the fringe field (dimensionless parameter corresponding to the parameter K1 in the code TRANSPORT, see remark 3).

    SK2: A second integral related to the extent of the fringe field (dimensionless parameter corresponding to the parameter K2 in the code TRANSPORT, see remark 3).

    APB(2): vertical half aperture at the exit (cm)


REMARKS:
 (1) The mid-plane field By (x, y = 0, t ) is expressed in terms of the dimensionless quantities n and β as:

     :math:`B_y\left(x,y=0,t\right) = B_y(0,0,t)\left[1-nhx+\beta h^2 x^2\right] , h= 1/\rho_0`

     :math:`n = - \left[\frac{1}{hB_y}\left(\frac{\partial B_y}{\partial x}\right)\right]_{x=y=0} , \beta = \left[\frac{1}{2h^2B_y}\left(\frac{\partial^2 B_y}{\partial x^2}\right)\right]_{x=y=0}`

 (2) The sign conventions are the ones in use in the code TRANSPORT (see SLAC Report-75):

    - A positive bend is to the right looking in the direction of particle travel, a negative one to the left. The ZROT card (see this type code for examples) may be used to represent bends in other directions.
    - In terms of sign conventions, to change the input parameters of a bending magnet set up for a proton beam to an equivalent H- beam and equivalent bending, one needs to change the sign on both ANGL and RMO
    - A positive sign of the angle of rotation on either entrance (PENT1) or exit (PENT2) pole-faces corresponds to a non- bend plane focusing action and bend plane defocusing action.
    - A positive sign of radius RAB1, RAB2 implies a convex curvature (it represents a negative sextupole component, see SLAC Report-75, page 71).

    The sign conventions are displayed in the following figure.
    
    .. image:: images/BMAGNET_f1.png

 (3) If APB(1) or/and APB(2) is null, the program inserts a default value zero for EK1 and EK2, as well as for SK1 and SK2.
     If APB(1) or/and APB(2) is not zero and EK1 or/and SK1 is negative, the program inserts a default value of EK1 = 0.5 or/and of SK1 = 0.5.

    Typical values of EK1/SK1 and EK2/SK2 are given below for four types of fringing boundaries:

        a) A linear drop-off of the field:

            `K1 = b/6g K2 = 3.8`
            where b is the extent of the linear fringe field and g is the vertical half aperture.

        b) A clamped “ROGOWSKI” fringe field: K1 = 0.4 K2 = 4.4
        c) An unclamped “ROGOWSKI” fringe field: K1 = 0.7 K2 = 4.4
        d) A square-edge non-saturating magnet: K1 = 0.45 K2 = 2.8

 (4) If in the input file BMAGNET_ is preceded by the combination of :ref:`gebeam`, :ref:`input` and :ref:`etac`, for space charge calculations one has to place the :ref:`scdynac` card after :ref:`etac`.
 (5) In the case of a multi-charge state beam, NSEC should be set to a value larger than 1 (at least 5 is advised).
 (6) In the case of space charge calculations, NSEC should be set to an even value larger than 1. **With a multi-charge state beam, only SCHEFF should be used for space charge calculations.**

EXAMPLES:

a) The case of a single-state charge beam and without space charge computations in a typical first-order transport for a wedge magnet whose total bend is 10 deg and bending radius of central trajectory is 40 cm:

::

    BMAGNET
    1 (NSEC)
    10. (ANGL in deg) 40. (RMO in cm) 0. (BAIM in kG) 0. (XN) 0. (XB)
    0. (PENT1 in deg) 0. (RAB1 in cm) 0.46(EK1) 2.75(EK2) 0. (APB(1))
    0. (PENT2 in deg) 0. (RAB2 in cm) 0.46(SK1) 2.75(SK2) 0. (APB(2))

b) The case of a multi-charge state beam and space charge computations in a bending magnet having the following characteristics:
    - Bend angle of central trajectory: 90 deg
    - Bending radius of the central trajectory: 100 cm
    - Field gradient: XN = 0.25
    - Second field gradient: XB = 0.25
    - Rotation angle of entrance and of exit face-poles: 27.71 deg
    - First fringe field coefficients at entrance and exit: EK1, SK1 = 0.46
    - Second fringe-field coefficient: EK2, SK2 = 2.75
    - Curvature of the entrance and exit pole-faces: RAB1, RAB2 = 27937 cm - Vertical half aperture at entrance and exit: APB(1), APB(2) = 3.3 cm:

::

    SECORD (initiate second-order matrix formalism)
    ...
    SCDYNAC (with 3, initiate space charge computations, see this Type Code)
    BMAGNET
    6 (NSEC)
    90. (ANGL in deg) 100. (RMO in cm) 0. (BAIM in kG) 0.25 (XN) 0.25 (XB)
    27.71(PENT1 in deg) 27937(RAB1 in cm) 0.46(EK1) 2.75(EK2) 3.3(APB (1))
    27.71(PENT2 in deg) 27937(RAB2 in cm) 0.46(SK1) 2.75(SK2) 3.3(APB (2))

DRIFT
-----

Transport through a free drift space

ENTRY:

 1) LD

    LD: length of the drift space (cm)

REMARKS:

 (1) Negative values can be used. In this case, no space charge computation is possible.
 (2) Space charge computations are automatically made with respect to the middle of the drift (see type code :ref:`scdynac`).
 (3) If the drift length is less than 0.00001 cm (i.e. 10E-7 m), space charge computation is automatically disabled in the drift.

SECORD
------

Following the SECORD card, second order transport matrix formalism in optical lenses is enabled.

ENTRY:

    none
