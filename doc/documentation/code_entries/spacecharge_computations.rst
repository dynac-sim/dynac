Space Charge Computations
=========================

.. _scdynac:

SCDYNAC
-------

**TODO: Complete this section...**

Select the space charge method and its parameters.

ENTRY:

    1) ISCSP

        ISCSP allows selecting the space charge method
        ISCSP = 1 or ISCSP = -1: HERSC (only to be used with non-relativistic beams) ISCSP = 2: SCHERM (only to be used with non-relativistic beams)
        ISCSP = 3: SCHEFF (can be used both with relativistic and non-relativistic beams)

    2) BEAMC SCE10

        BEAMC: electrical beam current (mA)
        SCE10 is a flag permitting to select elements in which space charge computations are made (see remark (3)). This can be of particular interest for the investigation of DTL type structures (i.e. Alvarez structures)
        SCE10=1: Space charge calculated for all relevant elements, but not at drifts. SCE10=2: Space charge calculated for accelerating elements only
        SCE10=3: Space charge calculated for all relevant elements
        The following lines depend on the space charge method selected

            A) Space charge routine HERSC (with ISCSP = 1 or ISCSP = -1):

                - If ISCSP = 1, the following entry will be read:

                    3) RDCF

                        RDCF is a fraction less or equal to one. It permits, when the number N of particles is large (i.e. N > 15000), the selection
                        of a reduced number N* = RDCF*N of particles for the computation of Hermite coefficients Almn (this allows saving computing time; see remark (1)).
                        If RDCF = 0, the code automatically defines this fraction.
                        No supplementary entry is required with ISCSP = 1, the default parameters in HERSC routine are in use (see below for the default parameters).
                        EXAMPLE:

                        ::

                            SCDYNAC
                            1 (ISCSP)
                            100. (BEAMC in mA) 3 (SCE10) 0. (RDCF)

                - If ISCSP = -1, the following entries 3), 4) and 5) will be read:

                    3) LMAXI MMAXI NMAXI

                        LMAXI, MMAXI and NMAXI are the upper limits of the Hermite series expansion representing the distribution of particles (see notes and remark (1)).
                        The default parameters in the code are: LMAXI = MMAXI = NMAXI = 11

                    4) FXRMS FYRMS FZRMS

                        Some few statistically isolated and very distant particles could affect the accuracy of the Hermite coefficients Almn . It is then recommended to remove these few "misplaced" particles. Therefore, HERMITE coefficients are computed from the particles included in a cube defined in RMS multiples (FXRMS, FYRMS and FZRMS) as follows in the x, y and z- directions:
                        Size of the cube in x-direction: (+ -)RMS(x) * FXRMS
                        Size of the cube in y-direction: (+ -)RMS(y) * FYRMS
                        Size of the cube in z-direction: (+ -)RMS(z) * FZRMS
                        where RMS(x), RMS(y) and RMS(z) are the horizontal, vertical and longitudinal RMS half beam sizes respectively. The default parameters in the code are: FXRMS = FYRMS = FZRMS = 2.5

                    5) EPS

                        Among the terms :math:`A_{lmn}\delta_{lmn}` emerging in the Hermite series expansion from the upper limits LMAXI, MMAXI and NMAXI, only a few tens of these are significant, the other terms can be removed. The parameter EPS allows removing these non-significant terms.
                        The default parameter in the code is: EPS = 8.E-03

