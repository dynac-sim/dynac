Input Beam
==========

.. _input:

INPUT
-----

Define energy and phase of the reference particle (i.e. the C.O.G. or the synchronous particle) at the entrance of the machine.

ENTRY:

 1) UEM ATM Q
    UEM: rest mass (MeV)

       Examples of rest mass:

           ===========   ============
           Particle      Mass
                         [MeV]
           ===========   ============
           Proton        938.27231
           H-            939.301404
           Mesons        33.9093
           Pions         139.5685
           Kaons         493.667
           Heavy ions    931.49432
           Electrons     0.511
           ===========   ============

    ATM: Atomic number

    Q: electrical charge state (in unit of electric charge)
 2) ENEDEP TOF

    ENEDEP: total kinetic energy of the particle reference at the input (MeV)

    TOF: time of flight, i.e. adjustment of the RF phase (deg) to be applied to particles

REMARKS:
 (1) INPUT card must be preceded by GEBEAM card. It is not used in combination with RDBEAM card.
 (2) At the start (after INPUT card), energy and time of flight of the COG and of the reference particle are coinciding and are considered disconnected (i.e. they will evolve separately, see type code REFCOG).
 (3) The file dynac_in_pr.dst will automatically be generated, containing the particle distribution at the input. Output is comparable to the usage of WRBEAM with 1 and 0 as its parameters for IREC and IFLAG respectively.

EXAMPLE: (entries for a 2.5 MeV H- beam)

::

    INPUT
    939.301404 (UEM [MeV]) 1. (ATM) -1. (Q)
    2.50 (ENEDEP [MeV]) 0. (TOF in deg)

EXAMPLE: (entries for a 0.25 MeV/u Pb27+ beam)

::

    INPUT
    931.49432(UEM [MeV]) 208. (ATM) 27. (Q)
    52. (ENEDEP [MeV]) 0. (TOF [deg])

.. _gebeam:

GEBEAM
------

**TODO finish this entry**

.. _rdbeam:

RDBEAM
------

**TODO finish this entry**

.. _etac:

ETAC
----

**TODO finish this entry**
