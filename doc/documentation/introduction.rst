
Introduction
============

Two different approaches in accelerating elements (i.e. cavities and gaps) exist in the code. First, a very fast analytical method based on the concept of an ‘equivalent field’ giving a full set of quasi-Liouvillian equations [1]_, only capable however of handle one single charge state. The second approach, which can handle several charge states in a bunch, is a relatively fast and very accurate step by step numerical method based on Bode’s rule applied to the ‘reduced coordinates’ resulting from a Picht transformation [1]_.

The analytical approach in accelerating elements (i.e. cavities and gaps) including new concepts has been introduced in DYNAC in 1994 giving a full set of quasi-Liouvillian equations and resulting in a convenient matrix formalism [1]_. Each of the 6D coordinates of the macro-particles is known in any position in the accelerating elements, thus allowing space charge computations in arbitrary positions in the cavities.

These equations are available both for non-relativistic electrons with significant acceleration as well as for heavy ions undergoing large velocity variations, albeit only for single charge state beams. A numerical method has been added to DYNAC in order to be able to simulate multi-charge state beams in accelerating elements.

Both methods are well suited to simulate particles, including non-relativistic electrons, accelerated through long accelerating elements with complex fields (e.g. complex helix, multi-gap cavities including superconducting ones) where their transit time may be of the order of 10p or more and where their velocities vary by 10% or more, or where their relativistic gamma varies by a factor 3 or 4. IH structures (where the design particle typically lies outside the beam) may be simulated.

The field of accelerating gaps and/or cavities can be read in the form of coordinates (z, E (z)), or it can be described by Transit Time Factors or by Fourier series expansion.

Apart from the above mentioned RF structures, also cavities of the buncher (in thin lens approximation)and RFQ (available for protons and heavy ions, [6]_ types are included in the code.

Furthermore a DC electron gun [7]_ as well as a Stripper, describing plural and multiple scattering of heavy ions in solids are included in the code [8]_, [9]_. Computations of synchrotron radiation (for electrons) in bending magnets are possible [10]_.

Three very different space charge routines [2]_, [3]_,[4]_, [5]_ are available in the code, which allows one to check the validity of space charge computations. These routines are:
 a) SCHEFF (a relativistic version developed at LANL, but further developed to handle multi-charge state beams)
 b) SCHERM (developed at CERN, CEA/SACLAY and LANL by P. Lapostolle et al; handles single charge state beams)
 c) HERSC (developed at CERN and CEA/SACLAY by P. Lapostolle et al; handles single charge state beams)

Apart from the usual optical lenses, special lenses such as a quadrupole associated with a sextupole field, a solenoid associated with a quadrupole field, steering magnets and solenoids with an arbitrary field as well as electrostatic quadrupoles and dipoles are included.

Second order transfer formalisms for most of the optical lenses are incorporated in the current version of the code; these elements can also handle multi-charge state beams.

As mentioned, heavy ion beams can be simulated, including multi-charge state ones. When using the plotting post- processor, multi-charge state beams will be color coded.

Misalignments and/or systematic or random defects in the matching parameters of cavities are possible. A physical acceptance of the machine can be defined.

The input beam can be generated from hit-or-miss Monte Carlo for different types of distributions or can be read in a file on the disk.

DYNAC can be compiled and run on Linux, MAC and MSWindows platforms. A GNU-plot based post-processor is also available, making graphics output possible on all three of the aforementioned platforms.

References
----------

.. [1] P.Lapostolle, E.Tanke and S.Valero: A New Method in Beam Dynamics Computations for Electrons and Ions in Complex Accelerating Elements, Particle Accelerators, 1994, Vol.44, pp. 215-255.
.. [2] F.Guy, Los Alamos Group AT-1 Memorandum AT-1:85-90, March 6, 1985
.. [3] P.Lapostolle and al.: A Modified Space Charge Routine for High Intensity Bunched Beams, NIM 379 (1996) pp. 21- 40.
.. [4] P.Lapostolle and al.: HERSC, A New 3D Space Charge Routine for High Intensity Bunched Beams, LINAC2002, Gyeongju, South Korea.
.. [5] P.Lapostolle, E.Tanke and S.Valero: A 3-d space charge routine, HERSC, based on a sequence of 3-d Hermite orthogonal functions (obtainable from the DYNAC website).
.. [6] C.Biscari: Computer programs and methods for the design of high intensity RFQ, CERN/PS 85-67 (LI) See also, E.Tanke and S.Valero: Motion of particles in a RFQ (obtainable from the DYNAC website).
.. [7] E. Tanke and S.Valero: Motion of electrons in a DC gun (obtainable from the DYNAC website). [8] D.A. Eastham: Plural and multiple scattering of heavy ions in solids, DL/NSF/P11.
.. [8] D.A. Eastham: Plural and multiple scattering of heavy ions in solids, DL/NSF/P11.
.. [9] D. Groom: Energy loss in matter by heavy particles, PDG-93-06.
.. [10] M.Sands: The Physics of Electron Storage Rings, SLAC-R121 See also, E.Tanke and S.Valero: Computation of synchronous radiations in bending magnets (obtainable from the DYNAC website).
